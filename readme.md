# CGI Recipe App
 
The is the backend spring project that analyzes the logs. The project was compile with Java 8. It uses a H2 database to store the log files.

### Rationale
The reason for dumping the file into a database is for ease of manupilation. Once the files are in a database, then it becomes easy to play with 
the data.

### Endpoints
There is a endpoint for uploading the log file. The services then writes valid log entries to the database. The user can use other endpoints to interogate the database. Endpoints are as follows:

http://localhost:8080/recipes/v1/upload - to upload  a file
http://localhost:8080/recipes/v1/all - to get all recipes in the table

### Test Coverage
As far as test coverage, there isnt much. 
