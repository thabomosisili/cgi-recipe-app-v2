package cgi.recipe.viewer.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import cgi.recipe.viewer.data.model.Recipe;
import cgi.recipe.viewer.data.service.RecipeService;
import cgi.recipe.viewer.storage.service.FileReaderService;
import cgi.recipe.viewer.storage.service.StorageService;


@RestController
@RequestMapping("/recipes/v1")
public class RecipeController {
	private final  StorageService storageService;
	private final  RecipeService recipeService;
	private final  FileReaderService fileReaderService;
	
	@Autowired
	public RecipeController(StorageService storageService,
			RecipeService recipeService,
			FileReaderService fileReaderService
			) {
		this.storageService = storageService;
		this.recipeService = recipeService;
		this.fileReaderService = fileReaderService;
	}
	
	@PostMapping("/upload")
	public ResponseEntity<?> handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {	
		Optional.ofNullable(file).orElseThrow(()-> new IllegalArgumentException("File Cannot be null"));
		fileReaderService.fileProcessor(storageService.store(file));
		return ResponseEntity.ok(HttpStatus.OK);
	}
	 @GetMapping("/all")
	  public List<Recipe> findAll() {
		return recipeService.findAllRecipes();
	  }

}
