package cgi.recipe.viewer.data.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;


import cgi.recipe.viewer.data.model.Recipe;
import cgi.recipe.viewer.data.repo.RecipeRepository;
import cgi.recipe.viewer.exception.RecipeNotFoundException;

@Service
@Transactional
public class RecipeService {
	
private final RecipeRepository recipeRepository;
	
	public RecipeService( RecipeRepository recipeRepository) {
		this.recipeRepository = recipeRepository;
	}
	
	public Recipe findRecipeById(Long id) {
        return recipeRepository.findRecipeById(id)
                .orElseThrow(() -> new RecipeNotFoundException("Log entry by id " + id + " was not found"));
    }

	  public Recipe addRecipe (Recipe recipe) {
	        return recipeRepository.save(recipe);
	    }

	    public List<Recipe> findAllRecipes() {
	        return recipeRepository.findAll();
	    }
	    
	    
	    @Transactional
		public List<Recipe> saveAllRecipes(List<Recipe> recipeList) {
			return recipeRepository.saveAll(recipeList);
		}

}
