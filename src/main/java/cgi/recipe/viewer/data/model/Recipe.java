package cgi.recipe.viewer.data.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "RECIPE")
public class Recipe {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGen")
	@SequenceGenerator(name = "seqGen", sequenceName = "seq", initialValue = 1)
	private Long id;
	private String title;
	private String href;
	private String ingredients;
	private String thumbnail;
	
	
	public Recipe(String title, String href, String ingredients, String thumbnail) {
		super();
		this.title = title;
		this.href = href;
		this.ingredients = ingredients;
		this.thumbnail = thumbnail;
	}
	
	
	public Recipe() {
		super();
	}


	public Long getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public String getHref() {
		return href;
	}
	public String getIngredients() {
		return ingredients;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	@Override
	public String toString() {
		return "Recipe [id=" + id + ", title=" + title + ", href=" + href + ", ingredients=" + ingredients
				+ ", thumbnail=" + thumbnail + "]";
	}


	
}
