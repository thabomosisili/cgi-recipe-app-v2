package cgi.recipe.viewer.data.repo;

import java.util.Optional;

import javax.persistence.Id;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import cgi.recipe.viewer.data.model.Recipe;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Id> {
	
	Optional<Recipe> findRecipeById(Long id);

}
