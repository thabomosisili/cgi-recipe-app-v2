package cgi.recipe.viewer.dto;

import java.util.List;

public class RecipeDTO {
	
	private String title;
	private String href;
	private List<String> ingredients;
	private String thumbnail;
	
	public String getTitle() {
		return title;
	}
	public String getHref() {
		return href;
	}
	public List<String> getIngredients() {
		return ingredients;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	 
	public void setTitle(String title) {
		this.title = title;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public void setIngredients(List<String> ingredients) {
		this.ingredients = ingredients;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	@Override
	public String toString() {
		return "RecipeJson [title=" + title + ", href=" + href + ", ingredients=" + ingredients + ", thumbnail="
				+ thumbnail + "]";
	}
 

}
