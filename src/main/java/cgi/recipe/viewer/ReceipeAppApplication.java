package cgi.recipe.viewer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import cgi.recipe.viewer.storage.service.StorageService;


@SpringBootApplication
public class ReceipeAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReceipeAppApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init(StorageService storageService) {
		return args -> {
			storageService.deleteAll();
			storageService.init();
		};
	}

}
