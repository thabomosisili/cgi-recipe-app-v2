package cgi.recipe.viewer.storage.service;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import cgi.recipe.viewer.data.model.Recipe;
import cgi.recipe.viewer.dto.RecipeDTO;

public interface FileReaderService {
	public List<Recipe> fileProcessor(Path source) throws IOException;


}
