package cgi.recipe.viewer.storage.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import cgi.recipe.viewer.data.model.Recipe;
import cgi.recipe.viewer.data.repo.RecipeRepository;
import cgi.recipe.viewer.data.service.RecipeService;
import cgi.recipe.viewer.dto.RecipeDTO;

 
@Component
public class FileReader implements FileReaderService {
	
	private final RecipeService recipeService;
	
	@Autowired
	public FileReader( RecipeService recipeService) {
		this.recipeService = recipeService;
	}
	 
	public List<Recipe> fileProcessor(Path source) throws IOException {
		List<Recipe> recipes = new ArrayList<>();
		try (InputStream inputStream = Files.newInputStream(source);
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				JsonReader reader = new JsonReader(new InputStreamReader(inputStream));
				) {
			reader.beginArray();
			while (reader.hasNext()) {
				RecipeDTO recipeDto = new Gson().fromJson(reader, RecipeDTO.class);
				Recipe recipe = new Recipe(recipeDto.getTitle(),
											recipeDto.getHref(),
											recipeDto.getIngredients().toString(),
											recipeDto.getThumbnail());
				recipes.add(recipe);	
				}
			recipeService.saveAllRecipes(recipes);
			reader.endArray();
		}catch (IOException e) {
	    	throw new RuntimeException("IOException opening file: " + source.toAbsolutePath().toString() + " " + e.getMessage());
		}
		 return recipes;
	}
 

}
