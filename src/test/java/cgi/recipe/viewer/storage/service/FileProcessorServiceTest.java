package cgi.recipe.viewer.storage.service;

import static org.junit.Assert.assertEquals;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringRunner.class)
public class FileProcessorServiceTest {
	
	@Autowired
	private FileReaderService fileReaderService;
	
	@Test
	public void testProcessRecords() throws Exception {
		
		Path currentDir = Paths.get("");
		String absolutePath = currentDir.toAbsolutePath()+"\\src\\test\\resources\\recipe.json";
		Path path = Paths.get(absolutePath);
		 fileReaderService.fileProcessor(path);
		//assertEquals(expectedResults,exceptionRecord); 
	}

}